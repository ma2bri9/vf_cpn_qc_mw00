


## Purpose
This script tries to speed up the **Quality Check** anlysis for VF CPN Project **MW00**

## Requirements
- Python 3.x
- openpyxl and ciscoconfparse libraries installed
- Access to **Mimir
---
## How to use

    ScriptDirectory
    │
    ├─── SITES ───+
                  ├── <SiteName1> ──+
                  │        .        │
                  │        .        +────── JSON ──────+
                  │        .        │                  │
                  │        .        │                  │─── <SiteName>.json            <─input_file
                  │        .        │
                  ├── <SiteNameN>   │
                  │                 +──── document ────+
                                    │                  │
                                    │                  │───<SiteMigrationTable.xls>    <─input_file
                                    │                  │───<VCE1.txt>                  <─input_file
                                    │                  │───<VCE2.txt>                  <─input_file
                                    │                  │───<VSW.txt>                   <─input_file
                                    │                  │───<VPE1.txt>                  <─input_file
                                    │                  │───<VPE2.txt>                  <─input_file
                                    │
                                    │───<SiteName>_MW00_report.xls                     <─output_file


** <SiteName>.json** example:
{
  "device_list" : [
    "VCE1",
    "VCE2",
    "VSW",
    "OSW1",
    "OSW2",
    "VPE1",
    "VPE2"],
  "username": "yourUsr",
  "password": "yourPwd"
}

In order to analyze the site **<SiteName>** you need to create:

- a **<SiteName>.json** file (as described above) inside the directory */ScriptDirectory/SITES/<SiteName>/JSON/*

- .txt files (<VCE1.txt>, <VCE2.txt>, <VSW.txt>, <VPE1.txt>, <VPE2.txt>) under the directory */ScriptDirectory/SITES/<SiteName>/document/* containing NIP and NMP pieces of configuration for each device

- MigrationTable (.xlsx) file under the directory */ScriptDirectory/SITES/<SiteName>/document/*

- execute **main.py**

At the end of excution, if nothing goes wrong, you get a <SiteName>_MW00_report.xls file under the directory */ScriptDirectory/SITES/<SiteName>/* containing a sheet for each analyzed device.
Each sheet is composed by a table with 3 coloums:

* the first one containing a flat description af a configuration rows;
* in the second one a number (1 or 2) indicating if that configuration line is in the live RunningConfig and in its documentation (2) or only in one of the two options (1)
* the third explicits the information in the second coloumn  indicating with "<DeviceName>_DEP" the live RunningConfig and with "<DeviceName>_DOC" the documantation