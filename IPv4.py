from functools import reduce
import re


class IPv4:
    a1 = 0
    a2 = 0
    a3 = 0
    a4 = 0

    def __init__(self, ipstr):
        if '.' not in ipstr:
            ipstr = '0.0.0.0'
        ip = ipstr.split('.')
        self.a1 = int(ip[0])
        self.a2 = int(ip[1])
        self.a3 = int(ip[2])
        self.a4 = int(ip[3])

    def __eq__(self, other):
        ss = [self.a1, self.a2, self.a3, self.a4]
        oo = [other.a1, other.a2, other.a3, other.a4]
        if ss == oo: return True
        return False

    def __cmp__(self, other):
        ss = [self.a1, self.a2, self.a3, self.a4]
        oo = [other.a1, other.a2, other.a3, other.a4]
        for i in range(3):
            if ss[i] > oo[i]: return 1
            if ss[i] < oo[i]: return -1
        return 0

    def __repr__(self):
        return str(self.a1) + '.' + str(self.a2) + '.' + str(self.a3) + '.' + str(self.a4)

    def getNetworkAddress(self, network):
        return str(self.a1 & network.a1) + '.' + str(self.a2 & network.a2) + '.' + str(
            self.a3 & network.a3) + '.' + str(self.a4 & network.a4)

    def isInside(self, ip, net):
        ip = ip.getNetworkAddress(net)
        if self.getNetworkAddress(net) == ip: return True
        return False

    def next(self):
        octs = [self.a1, self.a2, self.a3, self.a4 + 1]
        resto = 0
        octs.reverse()
        for i in range(len(octs)):
            octs[i] = octs[i] + resto
            resto = 0
            if octs[i] == 256:
                octs[i] = 0
                resto = 1
        octs.reverse()
        return IPv4(reduce(lambda x, y: x + '.' + y, map(lambda z: str(z), octs)))

    def getAddresses(self, net):
        net_a = [net.a1, net.a2, net.a3, net.a4]
        ip_a = [self.a1, self.a2, self.a3, self.a4]
        mask = reduce(lambda x, y: x + y, map(lambda z: len(re.findall('1', bin(z))), net_a))
        addressesN = 2 ** (32 - mask)
        network = list(range(4))
        for i, val in enumerate(range(4)):
            network[i] = ip_a[i] & net_a[i]
        network = reduce(lambda x, y: str(x) + '.' + str(y), network)
        network = IPv4(network)
        rangeAddress = []
        ip = network
        rangeAddress.append(ip)
        for i in range(addressesN - 1):
            ip = ip.next()
            rangeAddress.append(ip)
        return rangeAddress

    @staticmethod
    def masklen2IPv4(lenString):
        mlen = int(lenString)
        mlen = '1' * mlen + '0' * (32 - mlen)
        a1 = str(int(mlen[0:8], 2))
        a2 = str(int(mlen[8:16], 2))
        a3 = str(int(mlen[16:24], 2))
        a4 = str(int(mlen[24:32], 2))
        ipstring = a1 + '.' + a2 + '.' + a3 + '.' + a4
        return IPv4(ipstring)
