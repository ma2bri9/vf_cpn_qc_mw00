import re
from functools import reduce
import Run
import os
import openpyxl
from openpyxl import Workbook
import IPv4
import stage0
from ciscoconfparse import CiscoConfParse

def editConfig(text):

    def editIPv4(text):
        erun = Run.ConfigurationReader(text).idText.splitlines()
        erun2 = [x for x in erun if re.search('\d+\.\d+\.\d+\.\d+/\d+', x)]
        for x in erun2:
            y = re.search('(\d+\.\d+\.\d+\.\d+)/(\d+)', x)
            print('---', y.group(1), y.group(2))
            oldline = x.split(y.group(0))
            ip = IPv4.IPv4(y.group(1))
            mask = IPv4.IPv4.masklen2IPv4(y.group(2))
            erun.remove(x)
            erun.append(oldline[0]+str(ip)+' '+str(mask)+oldline[1])
        return Run.ConfigurationReader(
            lines_dic={i: Run.Line(i.split(Run.Line.getLabel())) for i in erun}).toTotalString(sortBy=True).lower()

    def getRangeInt(text):
        erun = Run.ConfigurationReader(text).idText.splitlines()
        erun2 = [x for x in erun if re.search('^interface ethernet\d/\d+-', x)]
        r = []
        for line in erun2:
            cline = Run.Line.string2Vector(line)
            h_cline = cline[0]
            l_cline = cline[1:]
            h_cline = h_cline.replace(',', '######interface').split('######')
            for intline in h_cline:
                x = re.search('(.*/)(\d+)-(\d+)', intline)
                base = x.group(1)
                low = int(x.group(2))
                high = int(x.group(3))
                print(base, low, high)
                for i in range(low, high + 1):
                    r.append(Run.Line.vector2String([base + str(i)] + list(l_cline)))
            erun.remove(line)
        erun = erun + r
        return Run.ConfigurationReader(
            lines_dic={i: Run.Line(i.split(Run.Line.getLabel())) for i in erun}).toTotalString(sortBy=True).lower()

    def editPortChannelMembersVlan(text):
        class PO:
            def __init__(self, id):
                self.id = id
                self.vlans = []
                self.members = []
        erun_text = Run.ConfigurationReader(text).idText
        erun = erun_text.splitlines()
        pos = [PO(x) for x in set(re.findall('channel-group (\d+) mode active', erun_text))]
        for po in pos:
            po.vlans = re.findall('^interface port-channel'+po.id+' .*switchport trunk allowed vlan (\d+)', erun_text, re.M)
            po.members = re.findall('^interface (\S+).*channel-group '+po.id+' mode active ', erun_text, re.M)
            print('@@@@@@@@@@@', po.id, po.vlans, po.members)
            for m in po.members:
                for v in po.vlans:
                    erun.append(Run.Line.vector2String(['interface '+m, 'switchport trunk allowed vlan '+v]))
        return Run.ConfigurationReader(lines_dic={i: Run.Line(i.split(Run.Line.getLabel())) for i in erun}).toTotalString(sortBy=True).lower()

    def vlanExplicit(text):
        erun = Run.ConfigurationReader(text).idText.splitlines()
        #x = [l.id for l in Run.ConfigurationReader(text).findByRegex('^interface.*(?:switchport|spanning).*vlan.*')] + \
        #    [l.id for l in Run.ConfigurationReader(text).findByRegex('^spanning.*vlan.*')] + \
        #    [l.id for l in Run.ConfigurationReader(text).findByRegex('^vlan.*')]
        x = [i for i in erun if re.search('vlan (\S+ )?([\d\-,]+)', i) and not (re.search('^vlan',i) and Run.Line.getLabel() in i)]

        for xx in x:
            #print('%%%%', xx)
            y_old = re.search('vlan (\S+ )?([\d\-,]+)', xx).group(2)
            y = y_old.split(',')
            y_withoout_ = [int(i) for i in y if '-' not in i]
            y_with_ = [list(range(int(i.split('-')[0]), int(i.split('-')[1]) + 1)) for i in y if '-' in i]
            y_with_ = reduce(lambda a, b: a + b, y_with_, [])
            y = y_withoout_ + y_with_
            y = [str(i) for i in y]
            y.sort()
            xx_before = xx.split(y_old)[0].replace(' add', '')
            xx_after = xx.split(y_old)[1]
            z = [xx_before + i + xx_after for i in y]
            #print('\n'.join(z))
            erun.remove(xx)
            erun = erun + z
        return Run.ConfigurationReader(lines_dic={i: Run.Line(i.split(Run.Line.getLabel())) for i in erun}).toTotalString(sortBy=True).lower()

    s = text
    s = vlanExplicit(s)
    s = getRangeInt(s)
    s = editPortChannelMembersVlan(s)
    s = editIPv4(s)
    return s


def read_sheet_from_workbook(workbook, sheet_name, FIRSTCELL):
    sheet = workbook[sheet_name]

    row_len = len(tuple(sheet.rows))
    column_len = len(tuple(sheet.columns))

    print(row_len, column_len)

    firstPosition = '#'
    for i in range(1, row_len):
        for j in range(1, column_len):
            if sheet.cell(i, j).value == FIRSTCELL:
                firstPosition = (i, j)
                break
    print(FIRSTCELL, '\tfirstPosition:', firstPosition)

    keys = []
    for i in sheet.iter_rows(min_row=firstPosition[0], max_row=firstPosition[0], min_col=firstPosition[1]):
        for j in i:
            keys.append(str(j.value))
    print('keys: ', keys)

    dict = {}
    for row in sheet.iter_rows(min_row=firstPosition[0] + 1, min_col=firstPosition[1]):
        dict_int = {}
        for index, cell in enumerate(row):
            dict_int[keys[index]] = str(cell.value)
        if dict_int[keys[0]]:
            dict[dict_int[keys[0]]] = dict_int

    return dict

def addPrompt(file_text=None, name=None):
    filestr = Run.ConfigurationReader(totalString=editConfig(file_text)).idText.splitlines()
    filestr = [name+'\t'+x for x in filestr]
    return '\n'.join(filestr)


def tidyUp(dir=None, filterList=None):
    class NetworkElement:
        def __init__(self, sho_run):
            self.name = sho_run.splitlines()[0].split('\t')[0]
            self.sho_run = '\n'.join([x.split('\t')[1] for x in sho_run.splitlines()])
            self.e_run = Run.ConfigurationReader(self.sho_run).idText.splitlines()

    device_list = []
    print(dir, os.listdir(dir))
    for device_file in os.listdir(dir):
        with open(os.path.join(dir,device_file)) as f:
            device_list.append(NetworkElement(f.read()))

    clines = []
    for device in device_list:
        clines = clines + device.e_run
    clines = list(set(clines))
    for f in filterList:
        print('@#@#',len(clines))
        clines = [x for x in clines if not re.search(f, x, re.M)]
    clines.sort()

    cdic = {}
    for c in clines:
        dev = []
        for device in device_list:
            if c in device.e_run:
                dev.append(device.name)
                dev.sort()
        cdic[c]=dev

    s = ''
    for c,d in cdic.items():
        s = s + c + '\t' + str(len(d)) + '\t' + str(d) + '\n'
    return s

def addNMPinfo(documentDIR, deployDIR, myPrivateDIR):
    nmp_file = [x for x in os.listdir(documentDIR) if '.xlsx' in x][0]
    nmp_wb = openpyxl.load_workbook(os.path.join(documentDIR, nmp_file))
    osws = [re.search('[A-Z]+OSW+\d+', x).group(0) for x in nmp_wb.sheetnames if re.search('OSW\d', x)]
    osws.sort()
    vces = [re.search('[A-Z]+VCE+\d+', x).group(0) for x in os.listdir(documentDIR) if 'VCE' in x]
    vces.sort()
    OSW1, OSW2 = osws
    VCE1, VCE2 = vces
    print(OSW1, OSW2, VCE1, VCE2)

    voice_vlans_sheet = [s for s in nmp_wb.sheetnames if 'Voice VLAN' in s][0]
    voice_vlans = read_sheet_from_workbook(nmp_wb, voice_vlans_sheet, 'VLAN ID')
    print('MARIO voice_vlans:', voice_vlans)

    stpInfo = read_sheet_from_workbook(nmp_wb, 'Higher STP Cost IF', 'Vlan')
    stpInfo = {k: v for k, v in stpInfo.items() if 'MP' in v['Type']}
    addToCouple1_vlans = [v['Vlan'] for v in stpInfo.values() if (v['ROOT_BRIDGE'] == OSW1) and (v['Vlan'] not in voice_vlans.keys())] + \
                         [v['VLAN ID'] for v in voice_vlans.values() if v['Set Root Bridge'].strip() == OSW1]
    print('- addToCouple1_vlans:', addToCouple1_vlans)
    addToCouple2_vlans = [v['Vlan'] for v in stpInfo.values() if (v['ROOT_BRIDGE'] == OSW2) and (v['Vlan'] not in voice_vlans.keys())] + \
                         [v['VLAN ID'] for v in voice_vlans.values() if v['Set Root Bridge'].strip() == OSW2]
    print('- addToCouple2_vlans:', addToCouple2_vlans)
    addToCouple1_txt = '\n\n\n'
    addToCouple1_txt = addToCouple1_txt + '''\n
interface port-channel69
 switchport
 switchport mode trunk
 mtu 9216
 no ip address
 load-interval 30
''' + '\n'.join(
        [' switchport trunk allowed vlan ' + str(x) for x in addToCouple1_vlans + addToCouple2_vlans]) + '\n\n'
    addToCouple1_txt = addToCouple1_txt + 'interface port-channel69\n' + '\n'.join(
        [' spanning-tree vlan ' + str(x) + ' cost 1000' for x in addToCouple2_vlans]) + '\n\n'
    ##addToCouple1_txt = addToCouple1_txt + '\n'.join(['spanning-tree vlan ' + str(x) + ' priority 24576' for x in addToCouple1_vlans]) +'\n\n'
    ##addToCouple1_txt = addToCouple1_txt + '\n'.join(['spanning-tree vlan ' + str(x) + ' priority 28672' for x in addToCouple2_vlans]) +'\n\n'
    addToCouple2_txt = '\n\n\n'
    addToCouple2_txt = addToCouple2_txt + '''
interface port-channel69
 switchport
 switchport mode trunk
 mtu 9216
 no ip address
 load-interval 30
'''  + '\n'.join(
        [' switchport trunk allowed vlan ' + str(x) for x in addToCouple1_vlans + addToCouple2_vlans]) + '\n\n'
    addToCouple2_txt = addToCouple2_txt + 'interface port-channel69\n' + '\n'.join(
        [' spanning-tree vlan ' + str(x) + ' cost 1000' for x in addToCouple1_vlans]) + '\n\n'
    # addToCouple2_txt = addToCouple2_txt + '\n'.join(['spanning-tree vlan ' + str(x) + ' priority 24576' for x in addToCouple2_vlans]) +'\n\n'
    # addToCouple2_txt = addToCouple2_txt + '\n'.join(['spanning-tree vlan ' + str(x) + ' priority 28672' for x in addToCouple1_vlans]) +'\n\n'
    addToOSWs_txt = '\n\n\n'
    addToOSWs_txt = addToOSWs_txt + '\n'.join(
        ['spanning-tree vlan ' + str(x) + ' priority 36864' for x in addToCouple1_vlans + addToCouple2_vlans]) + '\n\n'
    with open(os.path.join(myPrivateDIR, OSW1, '_'+OSW1 + '_doc.txt'), 'a') as f:
        f.write(addToCouple1_txt)
        f.write(addToOSWs_txt)
    with open(os.path.join(myPrivateDIR, OSW2, '_'+OSW2 + '_doc.txt'), 'a') as f:
        f.write(addToCouple2_txt)
        f.write(addToOSWs_txt)
    with open(os.path.join(myPrivateDIR, VCE1, '_'+VCE1 + '_doc.txt'), 'a') as f:
        f.write(addToCouple1_txt)
    with open(os.path.join(myPrivateDIR, VCE2, '_'+VCE2 + '_doc.txt'), 'a') as f:
        f.write(addToCouple2_txt)

    sheet = nmp_wb['Static Routes (OSW)']
    staticRoutes = []
    for row in sheet.iter_rows():
        cells = [c.value.strip() for c in row if c.value]
        if cells:
            staticRoutes.append(cells)
    print(staticRoutes)
    addToOSW1 = '\n\n' + '\n'.join([sr[1].replace('!', '').strip() for sr in staticRoutes if sr[0] == OSW1])
    with open(os.path.join(myPrivateDIR, OSW1, '_'+OSW1 + '_doc.txt'), 'a') as f:
        f.write(addToOSW1)
    addToOSW2 = '\n\n' + '\n'.join([sr[1].replace('!', '').strip() for sr in staticRoutes if sr[0] == OSW2])
    with open(os.path.join(myPrivateDIR, OSW2, '_'+OSW2 + '_doc.txt'), 'a') as f:
        f.write(addToOSW2)

    vrrpsheet = [sh for sh in nmp_wb.sheetnames if 'VRRP VIP & STATIC' in sh][0]
    print(vrrpsheet)
    vrrpData = read_sheet_from_workbook(nmp_wb, vrrpsheet, 'Net X')
    print('##########################################')
    print(vrrpData)
    print('##########################################')
    vrrpStaticRoutes = [v for v in vrrpData.values() if (v['Note'] == 'static' or v['Note'] == 'connected')]
    if vrrpStaticRoutes:
        oswsdeployfilevecotr = [x for x in os.listdir(deployDIR) if re.search('.*OSW.*', x)]
        oswsdeployfilevecotr.sort()
        osws = [d.split('.')[0] for d in oswsdeployfilevecotr]
        oswsIP3001 = ['10.190.41.220', '10.190.41.221']
        for index, osw in enumerate(oswsdeployfilevecotr):
            with open(os.path.join(deployDIR, osw)) as f:
                c = CiscoConfParse(f.readlines())
                print(c)
                po2osw = c.find_objects_w_child('^interface Port-channel', 'description.*OSW')[0].text[:-1]
                po2vpe = c.find_objects_w_child('^interface Port-channel', 'description.*VPE')[0].text[:-1]

                addToOSW_common = '\n\n\n' + '\n'.join(['vlan 3001', ' name TEMP_VLAN_ FOR_VOIP_MIGRATION',
                                   po2osw, ' switchport trunk allowed vlan 3001',
                                   po2vpe, ' switchport trunk allowed vlan 3001',
                                   'interface Vlan3001', ' description TEMP_SVI_ FOR_VOIP_MIGRATION', ' ip address ' + oswsIP3001[index] + ' 255.255.255.248'] + \
                                  ['ip route ' + s['Net X'] + ' ' + s['MASK'] + ' vlan3001 10.190.41.219 10' for s in vrrpStaticRoutes])

                with open(os.path.join(myPrivateDIR, osws[index], '_'+osws[index]+'_doc.txt'), 'a') as f:
                    f.write(addToOSW_common)
        #
        vpesdeployfilevecotr = [x for x in os.listdir(deployDIR) if re.search('.*VPE.*', x)]
        vpesdeployfilevecotr.sort()
        vpes = [d.split('.')[0] for d in vpesdeployfilevecotr]
        vpesIP3001 = ['10.190.41.217', '10.190.41.218']
        for index, vpe in enumerate(vpesdeployfilevecotr):
            with open(os.path.join(deployDIR, vpe)) as f:
                c = CiscoConfParse(f.readlines())
                print(c)
                po2osw = c.find_objects_w_child('^interface Bundle-Ether', 'description.*'+osws[index])[0].text[:-1]

                addToVPE_common = '\n\n\n' + '\n'.join(['prefix-set pfx_VOIP_CONNECTED_NOEXPORT', ' 255.255.255.255/32,', ' 10.190.41.216/29', 'end-set'] + \
                                  [po2osw+'.3001',
                                   ' description TEMP_SUBIF_FOR_VOIP_MIGRATION',
                                   ' mtu 1518',
                                   ' vrf VOIP',
                                   ' ipv4 address ' + vpesIP3001[index] + ' 255.255.255.248',
                                   ' encapsulation dot1q 3001'] + \
                                  ['router hsrp',
                                   ' ' + po2osw+'.3001',
                                   '  hsrp delay minimum 15 reload 200',
                                   '  address-family ipv4',
                                   '   hsrp 301',
                                   '    address 10.190.41.219'])

                with open(os.path.join(myPrivateDIR, vpes[index], '_'+vpes[index]+'_doc.txt'), 'a') as f:
                    f.write(addToVPE_common)

#DIR = os.getcwd()
DIR = stage0.get()
SITE = DIR.split(os.sep)[-1]
print('Site:', SITE)
deployDIR = os.path.join(DIR, 'deploy')
documentDIR = os.path.join(DIR, 'document')
myPrivateDIR = os.path.join(DIR, '_myPrivateDIR')
if not os.path.exists(myPrivateDIR):
    os.makedirs(myPrivateDIR)

print('deployDIR:', os.listdir(deployDIR))
print('documentDIR:', os.listdir(documentDIR))

#### create myProvateDIR and copy contents ####
for file in os.listdir(deployDIR):
    if '.xlsx' in file:
        continue
    device_name = file.split('.')[0]
    if not os.path.exists(os.path.join(myPrivateDIR, device_name)):
        os.makedirs(os.path.join(myPrivateDIR, device_name))
    with open(os.path.join(myPrivateDIR, device_name, '_'+device_name+'_dep.txt'), 'w') as f:
        with open(os.path.join(deployDIR, file)) as ff:
            f.write(ff.read())
for file in os.listdir(documentDIR):
    if '.xlsx' in file:
        continue
    device_name = file.split('.')[0]
    if not os.path.exists(os.path.join(myPrivateDIR, device_name)):
        os.makedirs(os.path.join(myPrivateDIR, device_name))
    with open(os.path.join(myPrivateDIR, device_name, '_'+device_name+'_doc.txt'), 'w') as f:
        with open(os.path.join(documentDIR,file)) as ff:
            f.write(ff.read())
#### add NMP infos ####
addNMPinfo(documentDIR, deployDIR, myPrivateDIR)
#### add prompt ####
for current_dir in os.listdir(myPrivateDIR):
    cdir = os.path.join(myPrivateDIR, current_dir)
    for file_name in os.listdir(cdir):
        cdirfile = os.path.join(cdir, file_name)
        text = ''
        with open(cdirfile) as f:
            text = f.read()
        with open(cdirfile, 'w') as f:
            f.write(addPrompt(text, re.search('_(.*)\.', file_name).group(1).upper()))

#############


workbook = Workbook()
for device_dir in os.listdir(myPrivateDIR):
    workbook.create_sheet(device_dir)
    ws = workbook[device_dir]
    filterList = ['^interface (?:ethernet|port-channel|vlan).*switchport $',
                  '^interface (?:ethernet|port-channel|vlan).*shutdown $',
                  '^interface (?:ethernet|port-channel|vlan).*switchport mode access $',
                  '^interface (?:ethernet|port-channel|vlan).*load-interval',
                  '^interface (?:ethernet|port-channel|vlan).*udld',
                  '^interface (?:ethernet|port-channel|vlan).*spanning-tree portfast $',
                  '^interface (?:ethernet|port-channel|vlan).*duplex full $',
                  '^!',
                  '^#',
                  'copp',
                  '.*md5.*',
                  '^rmon',
                  '^tacacs.*key',
                  '.*snmp.*']
    s = tidyUp(os.path.join(myPrivateDIR, device_dir), filterList=filterList)
    for ss in s.splitlines():
        ws.append(ss.split('\t'))
workbook.save(os.path.join(DIR, SITE+'_MW00_report.xlsx'))
workbook.close()