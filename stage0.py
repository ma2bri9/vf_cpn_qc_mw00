import requests
import json
import os
import re

def inputData():
    current_dir = os.getcwd()
    print('current_dir:', current_dir)
    sites_dir = os.path.join(current_dir, "SITES")
    for site in os.listdir(path=sites_dir):
        print(site)
    mysite = input("choose one: ")
    print('your choice:', mysite)
    mysite_dir = os.path.join(sites_dir, mysite)
    json_mysite_dir = os.path.join(mysite_dir, 'JSON')
    dic = {}
    with open(os.path.join(json_mysite_dir, mysite+'.json')) as f:
        dic.update(json.load(f))
        device_list = dic.pop('device_list')
        for device in device_list:
            device_dic = {}
            device_dic['device_name'] = device
            device_dic['site_dir'] = mysite_dir
            device_dic.update(dic)
            with open(os.path.join(json_mysite_dir, '_'+device+'.json'), 'w') as f:
                f.write(json.dumps(device_dic, ensure_ascii=False))
    return json_mysite_dir


class RoInf_Mimirized:
    """ Class to get cfg from mimir """

    def __init__(self, json_file=None, base_url="https://mimir-prod.cisco.com/api/mimir/np/"):
        self.__base_url__ = base_url
        self.json_file = json_file
        self.data_conn = self.__get_conn_data__()
        self.device_name = self.data_conn.pop('device_name')
        self.site_dir = self.data_conn.pop('site_dir')
        self.username = self.data_conn.pop('username')
        self.password = self.data_conn.pop('password')

    def __get_conn_data__(self):
        with open (self.json_file) as f:
            d = json.load(f)
            return d

    def __get_deviceID(self):
        url = self.__base_url__ + 'devices?cpyKey=70293&deviceName=' + self.device_name
        data = requests.get(url, auth=(self.username, self.password))
        data = json.loads(data.text)
        return data["data"][0]["deviceId"]

    def __get_running_conf__(self):
        url = self.__base_url__ + 'config?cpyKey=70293&deviceId=' + str(self.__get_deviceID())
        data = requests.get(url, auth=(self.username, self.password))
        data = json.loads(data.text)
        for elem in data["data"]:
            if elem["command"] == "running.config":
                return elem["rawData"]

    def save(self):
        fpath = os.path.join(self.site_dir, 'deploy', self.device_name+'.log')
        os.makedirs(os.path.dirname(fpath), exist_ok=True)
        if os.path.exists(fpath):
            print('File already exists')
        else:
            with open(fpath, "w") as f:
                print("Writing  ", fpath)
                f.write(self.__get_running_conf__())
                print("End writing")


def get():
    json_dir_list = inputData()
    print('json_dir_list:',json_dir_list)
    json_list = [os.path.join(json_dir_list,j) for j in os.listdir(path=json_dir_list) if re.search('^_', j)]
    for j in json_list:
        print(j)
        router = RoInf_Mimirized(j)
        print(router.device_name)
        router.save()
    print('SITE DIR:',router.site_dir)
    return router.site_dir
