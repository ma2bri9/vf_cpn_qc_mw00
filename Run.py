import re
from functools import reduce


class Line:
    @staticmethod
    def getLabel():
        #LABEL = ' \t'
        LABEL = ' -@- '
        return LABEL

    def __init__(self, lineArray, index=0, check=False):
        self.lineArray = tuple(map(lambda x: x.strip(), lineArray))
        self.line = self.lineArray[-1]
        self.index = index
        self.check = check
        if len(self.lineArray) > 1:
            self.parent = self.lineArray[-2]
        else:
            self.parent = None
        self.id = Line.vector2String(self.lineArray)

    @staticmethod
    def vector2String(vector):
        return reduce(lambda x, y: x.strip() + Line.getLabel() + y.strip(), vector) + ' '

    @staticmethod
    def string2Vector(string):
        return tuple(string.split(Line.getLabel()))


# prende una running-config e crea un dizionario con valore:Line chiave:Line.id
'''
class to map a running-configuration string in a dictionary with key=Line.id , value=Line
it can accept also a dictionary and return the running-configuration string
'''


class ConfigurationReader:
    def __init__(self, totalString='', lines_dic={}):
        if totalString:
            self.totalString = totalString
            self.lines_dic = {}
            self.lines_dic = self.__rewrite__()
            self.idText = reduce(lambda x, y: x + '\n' + y, list(self.lines_dic.keys()))
        elif lines_dic:
            self.lines_dic = lines_dic
            self.idText = reduce(lambda x, y: x + '\n' + y, list(self.lines_dic.keys()))
            self.totalString = self.toTotalString()
        else:
            self.lines_dic = lines_dic
            self.totalString = totalString
            self.idText = ''


    def __clearDicByKeys__(self, d, k):
        for i in list(d.keys()):
            if i >= k:
                del d[i]

    def __getPromp__(self, d):
        i = list(d.keys())
        i.sort()
        vector = list(map(lambda x: d[x], i))
        # vector = Line.vector2String(vector)
        return vector

    def __rewrite__(self):
        d = {}
        return_dic = {}
        for index, line in enumerate(self.totalString.splitlines()):
            if line.strip() == '!' or line.strip().upper()=='EXIT': ###########################
                continue
            i = re.search('\S', line)
            if i:
                i = i.start()
                self.__clearDicByKeys__(d, i)
                d[i] = line
                vector = self.__getPromp__(d)
                #lineObj = Line(line, index, vector)
                lineObj = Line(vector, index=index)
                return_dic[lineObj.id] = lineObj
        return return_dic

    '''
	take a string in input as a regular-expression and return a list of Line objects as the result of applying the regular-expression filter to the dictionary keys(=Line.id)
	'''

    def findByRegex(self, regex):
        print(repr(regex))
        l = re.findall(regex, self.idText, re.M)
        l = list(set(l))
        l.sort()
        l = list(map(lambda x: self.lines_dic.get(x), l))
        l = filter(lambda x: x is not None, l)
        return l

    '''
	take an array where each element is a string meaning as a regular-expression and return a list of Line objects as the result of applying the regular-expression filter to the dictionary keys(=Line.id) as an OR operation
	'''

    def findByRegex_vectorOR(self, regex_vector):
        ltot = []
        for rv in regex_vector:
            ltot = ltot + re.findall(rv, self.idText, re.M)
        ltot = list(set(ltot))
        ltot.sort()
        ltot = list(map(lambda x: self.lines_dic.get(x), ltot))
        ltot = filter(lambda x: x is not None, ltot)
        return ltot

    #########################################################################################
    @staticmethod
    def __min2vectors__(v_a, v_b):
        l = min(len(v_a), len(v_b))
        li = None
        for i in range(l):
            # print(v_a[i], v_b[i], v_a[i]==v_b[i])
            if v_a[i] != v_b[i]:
                li = i
                break
        if li == None:
            return l
        return li

    @staticmethod
    def __vector2strings_byIndex__(old_v, new_v):
        index = ConfigurationReader.__min2vectors__(old_v, new_v)

        # TAB = '    '
        TAB = ' '
        s = ''
        new_v = new_v[index:]
        #
        for i in range(len(old_v) - 1, index, -1):
            s = s + TAB * (i - 1) + '!\n'
            s = s + TAB * (i - 1) + 'exit\n'
        #
        for v_index, vs in enumerate(new_v):
            s = s + TAB * (index + v_index) + vs + '\n'
        return s

    ##maybe private method???
    def toTotalString(self, sortBy=False):  # sortBy: False = by index , True = by name
        if not sortBy:
            linesDicByIndex = {k: v for (k, v) in map(lambda x: (x[1].index, x[1]), self.lines_dic.items())}
        else:
            linesDicByIndex = self.lines_dic
        keysByIndex = list(linesDicByIndex.keys())
        keysByIndex.sort()

        totalString = ''
        line = None
        parentVector_withme_OLD = []
        for index in keysByIndex:
            line = linesDicByIndex[index]
            parentVector_withMe = line.lineArray
            totalString = totalString + ConfigurationReader.__vector2strings_byIndex__(parentVector_withme_OLD,
                                                                                       parentVector_withMe)
            parentVector_withme_OLD = parentVector_withMe
        return totalString
